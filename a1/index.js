/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
const promptGetUserInfo = function () {
	const fullName = prompt('What is your name?')
	const age = prompt('What is your age?')
	const loc = prompt('Where do you live?')

	console.log('Your name is ' + fullName)
	console.log('Your age is ' + age)
	console.log('You live in ' + loc)
}

promptGetUserInfo()

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
const displayFavBands = function () {
	console.log('1. The Beatles\n2.The Rolling Stones\n3.Pink Floyd\n4.Beach Boys\n5.Led Zeppelin')
}

displayFavBands()

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
const displayFavMovies = function () {
	console.log('1. M3GAN\nRotten Tomatoes Rating: 94%')
	console.log('2. Puss in Boots\nRotten Tomatoes Rating: 95%')
	console.log('3. The Menu\nRotten Tomatoes Rating: 88%')
	console.log('4. Missing\nRotten Tomatoes Rating: 84%')
	console.log('5. Avatar\nRotten Tomatoes Rating: 82%')
}

displayFavMovies()

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

// printUsers();
let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();

// console.log(friend1);
// console.log(friend2);
