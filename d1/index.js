console.log("Hello World");

//Mini-Activity
//log in the console your name 20 times

//Function
	//Functions in JS are lines/blocks of code that tell our device/application to perform a certain task when called/invoked

//Function Declarations
//function statement defines a function with the specified parameters

// Syntax
/*
	function functionName() {
		code block (statement)
	};

*/

//function keyword - used to defined a JS function
//functionName - functions are named to be able to use later in the code
//function block ({}) -	statements which comprise the body of the function
					// - this is where the code is to be executed	

function printname(){
	console.log("Cee");
}

printname();

//semicolons are used to separate executable JS statements

//function invocation

//call/invoke the functions that we declared
printname();


//printage(); //result in an error, much like variables, we cannot invoke a function we have yet to define

//function declaration vs expression

//function declaration
function declaredFunction() {
	console.log("Hello World from declaredFunction")
};
declaredFunction();

//function expression
//a function can also be stored in a variable. This is called a function variable

//a function expression is an anonymous function assigned to the variableFunction

//Anonymous function - a function without a name

let variableFunction = function(){
	console.log("Hello from variableFunction!");
}

variableFunction();

//a function expression of a function named funcName assigned to the variable funcExpression

//they are ALWAYS invoked (called) using the variable name 

let funcExpression = function funcName(){
	console.log("Hello From the Other Side!!!")
}

funcExpression();

//we can reassign declared functions and function expressions to new anonymous functions

declaredFunction = function(){
	console.log("updated declaredFunction");
}
declaredFunction();

funcExpression = function(){
	console.log("updated funcExpression");
}
funcExpression();


const constantFunc = function(){
	console.log("Initialized with const!")
}

constantFunc();

/*constantFunc = function(){
	console.log("Cannot be reassigned!")
}

constantFunc();*///Uncaught TypeError: Assignment to constant variable.


//Function Scoping

/*
	Scope is the accessibility of variables

	JavaScript variables has 3 types of scope:
	1. local/block scope
	2. global scope
	3. function scope
*/

	{
		let localVar = "Juan Dela Cruz";
	}

	let globalVar = "Mr. Worldwide";

	console.log(globalVar);
	//console.log(localVar);//result in error

	/*
		JS has a function scope:
		Each function creates a new scope
		Variables defined inside a function are NOT accessible (visible) from outside the function

	*/


	function showNames(){
		const functionConst = "John";
		let functionLet = "Jane";

		console.log(functionConst);
		console.log(functionLet);
	}

	showNames();

	//console.log(functionConst);//index.js:136 Uncaught ReferenceError: functionConst is not defined
	//console.log(functionLet);

//Nested Functions

	//we can create another function inside a function

	function myNewFunction(){

		let name = "Jane";

		function nestedFunction(){
			let nestedName = "John"
			console.log(name);
			//console.log(nestedName);
		}

		//console.log(nestedName);//results to an error
		//nestedName variable, being declared in the nestedFunction cannot be accessed outside of the function it was declared in
		nestedFunction();
	}

	myNewFunction();//Jane
	//nestedFunction();//returns an error
	//Moreover, since this function is declared inside myNewFunction, it too cannot be invoked outside of the function it was declared in


//Function and Global Scoped Variables

	let globalName = "Cardo";

	function myNewFunction2(){
		let nameInside = "Mario"

		console.log(globalName);
		//variables declared globally (outside any function) have a Global Scope
		//Global variables can be accessed from anywhere in a JS
		//program including from inside a function

	}

	myNewFunction2();//Cardo
	//console.log(nameInside);//error
		//nameInside is function-scoped. It cannot be accessed globally


//using alert()

//alert("Hello World");

function showSampleAlert(){
	alert("Hello User! Welcome to B248 Class!");
}
//showSampleAlert();

console.log("I will only log in the console when the alert is dismissed.");

//using prompt()
//the INPUT from the prompt() will be returned as a STRING once the user dismisses the window

let samplePrompt = prompt("Enter your Name!");

console.log("Hello, " + samplePrompt);

let sampleNullPrompt = prompt("Don't Enter Anything!")
console.log(sampleNullPrompt);//prompt() returns an empty string when there is no input, or null if the user cancels the prompt

//Sample

function printWelcomeMessage(){
	let firstName = prompt("Enter Your First Name");
	let lastName = prompt("Enter Your Last Name");
	alert("Thank you!");
	console.log("Hello, " + firstName + " " + lastName + "!");
	console.log("Welcome to B248 class!");
}

printWelcomeMessage();

//Function Naming Conventions

//should be definitive of the task it will perform
//it usually contains a verb

function getCourses(){
	let courses = ["Science","English","Math","Astrophysics"];
	console.log(courses);
}

getCourses();

//avoud pointless and inappropriate names

function pikachu(){
	let name = "Optimus Prime";
	console.log(name);
}

get();

//avoid generic names

function get(){

	let name = "Cardo Dalisay";
	console.log(name);
}

get();

//name your functions in small caps. follow camelCase when naming variables and functions

function displayCarInfo(){
	console.log("Brand: Toyota");
	console.log("Type: Sedan");
	console.log("Price: 1,500,000");
}

displayCarInfo();